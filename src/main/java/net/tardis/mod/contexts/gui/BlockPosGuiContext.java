package net.tardis.mod.contexts.gui;

import net.minecraft.util.math.BlockPos;
import net.tardis.mod.misc.GuiContext;

public class BlockPosGuiContext extends GuiContext {

	public BlockPos pos;
	
	public BlockPosGuiContext(BlockPos pos) {
		this.pos = pos;
	}
}
