package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.TRenderTypes;
import net.tardis.mod.client.models.exteriors.PoliceBoxExteriorModel;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.exteriors.PoliceBoxExteriorTile;

/**
 * Created by 50ap5ud5
 * on 18 Apr 2020 @ 12:55:35 pm
 */
public class PoliceBoxExteriorRenderer extends ExteriorRenderer<PoliceBoxExteriorTile>{
    
    public PoliceBoxExteriorRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
        super(rendererDispatcherIn);
    }

//    private static BotiManager boti = new BotiManager();
    
    public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, 
            "textures/exteriors/police_box.png"); 
    
    private PoliceBoxExteriorModel model = new PoliceBoxExteriorModel();
    public static WorldText TEXT = new WorldText(0.75F, 0.75F, 1, 0xFFFFFF);
    
    @Override
    public void renderExterior(PoliceBoxExteriorTile tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn, float alpha) {
        matrixStackIn.push();
        
        float scale = 0.5F;
        matrixStackIn.translate(0, -0.25, 0); //This renders the box upwards on the y axis by 0.25
        matrixStackIn.scale(scale, scale, scale); //Scales the model down by 2
        
        this.model.render(tile, scale, matrixStackIn, bufferIn.getBuffer(TRenderTypes.getTardis(TEXTURE)), combinedLightIn, combinedOverlayIn, alpha);
        matrixStackIn.pop();

        //Front
        matrixStackIn.push();
        matrixStackIn.translate(-0.45, -1.66, -11.3 / 16.0F);
        TEXT.renderText(matrixStackIn, bufferIn, combinedLightIn, tile.getCustomName());
        matrixStackIn.pop();
        
        //Left text
        matrixStackIn.push();
        matrixStackIn.translate(-0.715, -1.66, 7.25 / 16.0F);
        matrixStackIn.rotate(Vector3f.YP.rotationDegrees(90));
        TEXT.renderText(matrixStackIn, bufferIn, combinedLightIn, tile.getCustomName());
        matrixStackIn.pop();
        
        //Right text
        matrixStackIn.push();
        matrixStackIn.translate(0.715, -1.66, -7.25 / 16.0F);
        matrixStackIn.rotate(Vector3f.YN.rotationDegrees(90));
        TEXT.renderText(matrixStackIn, bufferIn, combinedLightIn, tile.getCustomName());
        matrixStackIn.pop();
        
        //BACK text
        matrixStackIn.push();
        matrixStackIn.translate(0.45, -1.66, 11.3 / 16.0F);
        matrixStackIn.rotate(Vector3f.YP.rotationDegrees(180));
        TEXT.renderText(matrixStackIn, bufferIn, combinedLightIn, tile.getCustomName());
        matrixStackIn.pop();
        
        
    }

}
