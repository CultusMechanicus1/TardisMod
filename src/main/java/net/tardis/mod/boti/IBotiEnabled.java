package net.tardis.mod.boti;

import net.tardis.mod.network.packets.BOTIMessage;

public interface IBotiEnabled {

    WorldShell getBotiWorld();

    void setBotiWorld(WorldShell shell);

    BOTIMessage createMessage(WorldShell shell);

}
