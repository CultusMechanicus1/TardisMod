package net.tardis.mod.subsystem;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.nbt.CompoundNBT;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.SparkingLevel;

public class StabilizerSubsystem extends Subsystem{
	
	private boolean activated;
	private int seconds = 0;

	public StabilizerSubsystem(ConsoleTile console, Item item) {
		super(console, item);
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = super.serializeNBT();
		tag.putBoolean("control_activated", this.activated);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		super.deserializeNBT(nbt);
		this.activated = nbt.getBoolean("control_activated");
	}

	@Override
	public void onTakeoff() {}

	@Override
	public void onLand() {}

	@Override
	public void onFlightSecond() {
		if(!console.getWorld().isRemote && this.isControlActivated()) {
			++this.seconds;
			if(this.seconds >= 10) {
				this.seconds = 0;
				this.damage((ServerPlayerEntity)console.getPilot(), 1);
			}
		}
			
	}
	/** If if this stabiliser control is enabled*/
	public boolean isControlActivated() {
		
		if(!this.canBeUsed())
			this.activated = false;
		
		return this.activated;
	}
    /** Set the stabiliser to be turned on*/
	public void setControlActivated(boolean b) {
		if(!this.canBeUsed())
			this.activated = false;
		else this.activated = b;
		this.console.updateClient();
	}

	@Override
	public SparkingLevel getSparkState() {
		return SparkingLevel.NONE;
	}

	@Override
	public boolean stopsFlight() {
		return false;
	}

}
