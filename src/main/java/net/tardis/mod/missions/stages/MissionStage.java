package net.tardis.mod.missions.stages;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Util;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.registries.ForgeRegistryEntry;
/** Object to allow a mission to have multiple objectives
 * <p> Objective - Tasks that the player must complete. E.g. Killing 1 entity
 * <br> Stage - A collection of Objectives. 
 * <p> Each Mission can have multiple stages. Each stage can have many objectives*/
public class MissionStage extends ForgeRegistryEntry<MissionStage> implements INBTSerializable<CompoundNBT>{
	private int maxObjectives = 1;
	private int currentObjectiveIndex = 0;
	private String translationKey;
	
	public MissionStage(int maxObjectives) {
		this.maxObjectives = maxObjectives;
	}
	
	public MissionStage() {
		this(1);
	}
	
	public int getMaxObjectives() {
		return this.maxObjectives;
	}
	
	public MissionStage setMaxObjectives(int maxObjectives) {
		this.maxObjectives = maxObjectives;
		return this;
	}
	
	public int getCurrentObjectiveIndex() {
		return this.currentObjectiveIndex;
	}
	
	public MissionStage setObjectiveIndex(int index) {
		this.currentObjectiveIndex = index;
		return this;
	}
	/** Resets the stage objective index to 0. Must be called when switching to a new stage*/
	public MissionStage resetObjectiveIndex() {
		return setObjectiveIndex(0);
	}
	/** If this stage is complete*/
	public boolean isComplete() {
		return this.currentObjectiveIndex >= this.maxObjectives;
	}
	
	public String getTranslationKey() {
		if (translationKey == null) {
			translationKey = Util.makeTranslationKey("mission_stage", this.getRegistryName());
		}
		return translationKey;
	}
	/** Get default display name*/
	public TranslationTextComponent getDisplayName() {
		return new TranslationTextComponent(this.getTranslationKey());
	}
	
	public IFormattableTextComponent getDisplayNameForCurrentObjective() {
		return this.getDisplayNameForObjective(this.currentObjectiveIndex);
	}
	/** Handle the display name based on different objective indexes */
	protected IFormattableTextComponent getDisplayNameForObjective(int objective) {
		return getDisplayName();
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
        tag.putInt("max_objectives", this.maxObjectives);
        tag.putInt("current_objective", this.currentObjectiveIndex);
        return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.maxObjectives = nbt.getInt("max_objectives");
		this.currentObjectiveIndex = nbt.getInt("current_objective");
	}

}
