package net.tardis.mod.registries;

import java.util.function.Supplier;

import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.items.TItems;
import net.tardis.mod.subsystem.AntennaSubsystem;
import net.tardis.mod.subsystem.ChameleonSubsystem;
import net.tardis.mod.subsystem.FlightSubsystem;
import net.tardis.mod.subsystem.FluidLinksSubsystem;
import net.tardis.mod.subsystem.NavComSubsystem;
import net.tardis.mod.subsystem.ShieldGeneratorSubsystem;
import net.tardis.mod.subsystem.StabilizerSubsystem;
import net.tardis.mod.subsystem.SubsystemEntry;
import net.tardis.mod.subsystem.TemporalGraceSubsystem;

public class SubsystemRegistry {
	
    public static final DeferredRegister<SubsystemEntry> SUBSYSTEMS = DeferredRegister.create(SubsystemEntry.class, Tardis.MODID);
	
	public static Supplier<IForgeRegistry<SubsystemEntry>> SUBSYSTEM_REGISTRY = SUBSYSTEMS.makeRegistry("subsystem", () -> new RegistryBuilder<SubsystemEntry>().setMaxID(Integer.MAX_VALUE - 1));
	
	public static final RegistryObject<SubsystemEntry> FLIGHT = SUBSYSTEMS.register("flight", () ->  new SubsystemEntry(FlightSubsystem::new, TItems.DEMAT_CIRCUIT.get()));
	public static final RegistryObject<SubsystemEntry> FLUID_LINKS = SUBSYSTEMS.register("fluid_link", () ->  new SubsystemEntry(FluidLinksSubsystem::new, TItems.FLUID_LINK.get()));
	public static final RegistryObject<SubsystemEntry> CHAMELEON = SUBSYSTEMS.register("chameleon", () ->  new SubsystemEntry(ChameleonSubsystem::new, TItems.CHAMELEON_CIRCUIT.get()));
	public static final RegistryObject<SubsystemEntry> ANTENNA = SUBSYSTEMS.register("antenna", () ->  new SubsystemEntry(AntennaSubsystem::new, TItems.INTERSTITIAL_ANTENNA.get()));
	public static final RegistryObject<SubsystemEntry> TEMPORAL_GRACE = SUBSYSTEMS.register("temporal_grace", () ->  new SubsystemEntry(TemporalGraceSubsystem::new, TItems.TEMPORAL_GRACE.get()));
	public static final RegistryObject<SubsystemEntry> SHIELD_GENERATOR = SUBSYSTEMS.register("shield_generator", () ->  new SubsystemEntry(ShieldGeneratorSubsystem::new, TItems.SHEILD_GENERATOR.get()));
	public static final RegistryObject<SubsystemEntry> STABILIZERS = SUBSYSTEMS.register("stabilizers", () ->  new SubsystemEntry(StabilizerSubsystem::new, TItems.STABILIZERS.get()));
	public static final RegistryObject<SubsystemEntry> NAV_COM = SUBSYSTEMS.register("nav_com", () ->  new SubsystemEntry(NavComSubsystem::new, TItems.NAV_COM.get()));
}
