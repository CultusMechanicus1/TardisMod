package net.tardis.mod.subsystem;

import net.minecraft.item.Item;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundCategory;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.SparkingLevel;

public class NavComSubsystem extends Subsystem{

	public NavComSubsystem(ConsoleTile console, Item item) {
		super(console, item);
	}

	@Override
	public CompoundNBT serializeNBT() {
		return super.serializeNBT();
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		super.deserializeNBT(nbt);
	}

	@Override
	public void onTakeoff() {}

	@Override
	public void onLand() {}

	@Override
	public void onFlightSecond() {}

	@Override
	public boolean stopsFlight() {
		return false;
	}

	@Override
	public SparkingLevel getSparkState() {
		return SparkingLevel.NONE;
	}
	
	public void playDenySound(ConsoleTile console) {
		if(console.getWorld() != null)
			console.getWorld().playSound(null, console.getPos(), TSounds.SONIC_FAIL.get(), SoundCategory.BLOCKS, 1.0F, 1.0F);
	}

}
