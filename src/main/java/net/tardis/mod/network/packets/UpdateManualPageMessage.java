package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.items.TItems;

public class UpdateManualPageMessage {
	
	public int page;
	public int chapter;
	
	public UpdateManualPageMessage(int page, int chapter) {
		this.page = page;
		this.chapter = chapter;
	}
	
	public static void encode(UpdateManualPageMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.page);
		buf.writeInt(mes.chapter);
	}
	
	public static UpdateManualPageMessage decode(PacketBuffer buf) {
		return new UpdateManualPageMessage(buf.readInt(), buf.readInt());
	}

	public static void handle(UpdateManualPageMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			PlayerEntity ent = cont.get().getSender();
			if(ent.getHeldItemMainhand().getItem() == TItems.MANUAL.get()) {
				CompoundNBT tag = ent.getHeldItemMainhand().hasTag() ? ent.getHeldItemMainhand().getTag() : new CompoundNBT();
				tag.putInt("page", mes.page);
				tag.putInt("chapter", mes.chapter);
				ent.getHeldItemMainhand().setTag(tag);
			}
		});
		cont.get().setPacketHandled(true);
	}
}
