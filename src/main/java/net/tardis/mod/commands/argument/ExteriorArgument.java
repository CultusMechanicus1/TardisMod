package net.tardis.mod.commands.argument;

import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.arguments.ArgumentType;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.DynamicCommandExceptionType;
import com.mojang.brigadier.suggestion.Suggestions;
import com.mojang.brigadier.suggestion.SuggestionsBuilder;

import net.minecraft.command.CommandSource;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.exterior.AbstractExterior;
import net.tardis.mod.registries.ExteriorRegistry;

public class ExteriorArgument implements ArgumentType<ResourceLocation>{
	private static final Collection<String> EXAMPLES = Stream.of(ExteriorRegistry.STEAMPUNK, ExteriorRegistry.TRUNK).map((exterior) -> {
	      return exterior.get().getRegistryName().toString();
	   }).collect(Collectors.toList());
    private static final DynamicCommandExceptionType INVALID_EXTERIOR_EXCEPTION = new DynamicCommandExceptionType((exterior) -> {
      return new TranslationTextComponent("argument.tardis.exterior.invalid", exterior);
    });
	
	@Override
	public ResourceLocation parse(StringReader reader) throws CommandSyntaxException {
		return ResourceLocation.read(reader);
	}
	
	@Override
	public <S> CompletableFuture<Suggestions> listSuggestions(CommandContext<S> context, SuggestionsBuilder builder) {
		Collection<ResourceLocation> exteriors = ExteriorRegistry.EXTERIOR_REGISTRY.get().getKeys();
		return context.getSource() instanceof ISuggestionProvider ? ISuggestionProvider.func_212476_a(exteriors.stream(), builder) : Suggestions.empty();
	}

	@Override
	public Collection<String> getExamples() {
		return EXAMPLES;
	}

	public static ExteriorArgument getExteriorArgument() {
	      return new ExteriorArgument();
	}
	
	public static AbstractExterior getExterior(CommandContext<CommandSource> context, String name) throws CommandSyntaxException {
		ResourceLocation resourcelocation = context.getArgument(name, ResourceLocation.class);
		AbstractExterior room = ExteriorRegistry.EXTERIOR_REGISTRY.get().getValue(resourcelocation);
		if (room == null)
			throw INVALID_EXTERIOR_EXCEPTION.create(resourcelocation);
		else
			return room;
	}

}
