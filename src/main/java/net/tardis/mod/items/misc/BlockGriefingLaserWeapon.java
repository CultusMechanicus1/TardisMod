package net.tardis.mod.items.misc;

import net.minecraft.block.Block;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.Explosion;
import net.minecraft.world.GameRules;
import net.tardis.mod.damagesources.TDamageSources;
import net.tardis.mod.entity.projectiles.LaserEntity;
import net.tardis.mod.sounds.TSounds;

public class BlockGriefingLaserWeapon<T extends LivingEntity> extends GenericLaserWeapon<T>{
    protected Class<? extends Block> blockToDestroy;

    public BlockGriefingLaserWeapon(T user, DamageSource source, float damage, Vector3d colour, float rayLength, SoundEvent sound) {
        super(user, source, rayLength, colour, rayLength, sound);
    }
    
    public BlockGriefingLaserWeapon(T user) {
        this(user, TDamageSources.causeTardisMobDamage(TDamageSources.LASER, user), 5F, new Vector3d(0, 1, 1), 1.0F, TSounds.LASER_GUN_FIRE.get());
    }
    
    @Override
    public void onHit(LaserEntity laserEntity, RayTraceResult result) {
    	BlockPos pos = new BlockPos(result.getHitVec());
        GameRules.BooleanValue shouldGrief = laserEntity.world.getGameRules().get(GameRules.MOB_GRIEFING);
        if (pos != null && shouldGrief.get()) {
           if (blockToDestroy != null) {
        	   if (laserEntity.getEntityWorld().getBlockState(pos).getBlock().getClass().equals(blockToDestroy)) {
        		   laserEntity.world.createExplosion(laserEntity, source, null, pos.getX(), pos.getY(), pos.getZ(), 1F, false, Explosion.Mode.DESTROY);
        	   }
           }
        }
    }
    
    public void setBlockToDestroy(Class<? extends Block> blockToDestroy) {
        this.blockToDestroy = blockToDestroy;
    }
    
    
}
