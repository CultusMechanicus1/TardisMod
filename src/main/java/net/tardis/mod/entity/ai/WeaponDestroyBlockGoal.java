package net.tardis.mod.entity.ai;

import java.util.function.Predicate;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.Difficulty;
import net.minecraft.world.World;
import net.tardis.mod.items.misc.BlockGriefingLaserWeapon;
import net.tardis.mod.misc.AbstractWeapon;
/** Generic Goal to allow a particular weapon to cause a block to destroy blocks. Usually use the GenericLaserWeapon for this*/
public class WeaponDestroyBlockGoal<E extends LivingEntity> extends Goal{
	protected AbstractWeapon<E> weapon;
	private final Predicate<Difficulty> difficultyPredicate;
	protected final Class<? extends Block> blockToDestroy;
	protected final E entity;
	protected final double distanceToBlock;
	protected BlockPos blockPos = BlockPos.ZERO;
	protected int timeout = 20;
	
	public WeaponDestroyBlockGoal(Class<? extends Block> blockToDestroy, E weaponHolder, AbstractWeapon<E> weapon, double distanceToBlock, Predicate<Difficulty> difficultyPredicate) {
	    this.blockToDestroy = blockToDestroy;
	    this.entity = weaponHolder;
	    this.weapon = weapon;
	    this.distanceToBlock = distanceToBlock;
	    this.difficultyPredicate = difficultyPredicate;
	}
	
	public WeaponDestroyBlockGoal(Class<? extends Block> blockToDestroy, E weaponHolder, double distanceToBlock, Predicate<Difficulty> difficultyPredicate) {
	    this(blockToDestroy, weaponHolder, new BlockGriefingLaserWeapon<E>(weaponHolder), distanceToBlock,difficultyPredicate);
	}

	@Override
	public boolean shouldExecute() {
		if (!this.entity.world.isRemote() && this.blockPos == BlockPos.ZERO && timeout > 0) {
	        return this.canBreakOnDifficulty(this.entity.world.getDifficulty());
		}
		return false;
	}
	
	@Override
	public boolean shouldContinueExecuting() {
		if (!this.entity.world.isRemote()) {
			if (blockPos != BlockPos.ZERO) {
			    if (this.doesBlockStillExist(entity.world, blockPos) && timeout > 0)
			        return true;
			}
		}
		return false;
	}
	
	@Override
	public void startExecuting() {
		super.startExecuting();
	}
	
	@Override
	public void resetTask() {
		super.resetTask();
		this.blockPos = BlockPos.ZERO;
		this.timeout = 20;
	}

	@Override
	public void tick() {
		super.tick();
		if (!entity.world.isRemote()) {
			if(this.timeout > 0)
	            --this.timeout;
			if (entity.world.getGameTime() % 20 == 0) {
				BlockRayTraceResult target = this.findTarget(entity, distanceToBlock, 0, false);
				if (target != null) {
				    if (target.getPos() != null) {
				    	this.blockPos = target.getPos();
				        BlockState state = entity.world.getBlockState(target.getPos());
				        if (this.matchesTargetBlock(state)) {
				            if (this.weapon != null && timeout > 0) {
				                this.weapon.useWeapon(entity);
				                this.resetTask();
				            }
				        }
				    }
				}	
			}
		}
	}
	
	protected boolean matchesTargetBlock(BlockState state) {
	    if (state != null) {
	    	return state.getBlock().getClass().equals(this.blockToDestroy);
	    }
	    return false;
	}

	private boolean canBreakOnDifficulty(Difficulty difficulty) {
	    return this.difficultyPredicate.test(difficulty);
	}
	
	protected BlockRayTraceResult findTarget(LivingEntity dalek, double distanceToBlock, float partialTicks, boolean searchThroughFluids) {
	    Vector3d eyeVec = dalek.getEyePosition(1.0F);
	    Vector3d lookVec = dalek.getLook(partialTicks);
	    Vector3d searchVec = eyeVec.add(lookVec.x * distanceToBlock, lookVec.y * distanceToBlock, lookVec.z * distanceToBlock);
	    return dalek.world.rayTraceBlocks(new RayTraceContext(eyeVec, searchVec, RayTraceContext.BlockMode.OUTLINE, searchThroughFluids ? RayTraceContext.FluidMode.ANY : RayTraceContext.FluidMode.NONE, dalek));
	}
	
	protected boolean doesBlockStillExist(World world, BlockPos pos) {
	    return this.matchesTargetBlock(world.getBlockState(pos));
	}
	
	public WeaponDestroyBlockGoal<E> setWeaponType(AbstractWeapon<E> doorBreakingWeapon) {
	    this.weapon = doorBreakingWeapon;
	    return this;
	}

}
