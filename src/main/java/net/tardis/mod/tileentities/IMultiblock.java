package net.tardis.mod.tileentities;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;

public interface IMultiblock {

	BlockPos getMaster();
	BlockState getMasterState();
	MultiblockMasterTile getMasterTile();
	void setMasterPos(BlockPos pos);
}
