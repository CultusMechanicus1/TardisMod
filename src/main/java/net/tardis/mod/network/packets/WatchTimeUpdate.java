package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;

public class WatchTimeUpdate {

	private int variant;
	private int slot;
	
	public WatchTimeUpdate(int slot, int variant) {
		this.slot = slot;
		this.variant = variant;
	}
	
	
	public static void encode(WatchTimeUpdate mes, PacketBuffer buf) {
		buf.writeInt(mes.slot);
		buf.writeInt(mes.variant);
	}
	
	public static WatchTimeUpdate decode(PacketBuffer buf) {
		return new WatchTimeUpdate(buf.readInt(), buf.readInt());
	}
	
	public static void handle(WatchTimeUpdate update, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> handleClient(update.slot, update.variant));
		cont.get().setPacketHandled(true);
	}
	
	@OnlyIn(Dist.CLIENT)
	public static void handleClient(int slot, int variant) {
		PlayerEntity player = Minecraft.getInstance().player;
		if(player != null) {
			ItemStack stack = player.inventory.getStackInSlot(slot);
			stack.getCapability(Capabilities.WATCH_CAPABILITY).ifPresent(cap -> {
				cap.setVariant(variant);
			});
		}
	}
}