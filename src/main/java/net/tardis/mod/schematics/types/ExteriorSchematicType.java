package net.tardis.mod.schematics.types;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.schematics.ExteriorUnlockSchematic;
import net.tardis.mod.schematics.Schematic;

public class ExteriorSchematicType extends SchematicType{

    @Override
    public Schematic deserialize(JsonObject root) {
        ExteriorUnlockSchematic schematic = new ExteriorUnlockSchematic(this);
        schematic.setExterior(new ResourceLocation(root.get("result").getAsString()));
        setTranslationOrDisplayName(root, schematic);
        return schematic;
    }

    @Override
    public JsonObject serialize(Schematic schematic) {
        ExteriorUnlockSchematic extSchematic = this.getSchematicAs(ExteriorUnlockSchematic.class, schematic);

        JsonObject root = this.createBaseJson(schematic.getDisplayName(), schematic.isUsingTranslatedName());
        root.add("result", new JsonPrimitive(extSchematic.getExterior().toString()));
        return root;
    }
    
    @Override
    public Schematic deserialize(PacketBuffer buffer) {
        ExteriorUnlockSchematic schematic = new ExteriorUnlockSchematic(this);
        schematic.setExterior(buffer.readResourceLocation());
        return schematic;
    }

    @Override
    public Schematic serialize(Schematic schematic, PacketBuffer buffer) {
        ExteriorUnlockSchematic schem = this.getSchematicAs(ExteriorUnlockSchematic.class, schematic);
        buffer.writeResourceLocation(schem.getExterior());
        return schem;
    }

}
