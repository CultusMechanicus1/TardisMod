package net.tardis.mod.entity.ai.dalek;

import java.util.function.Predicate;

import net.minecraft.block.Block;
import net.minecraft.world.Difficulty;
import net.tardis.mod.entity.ai.WeaponDestroyBlockGoal;
import net.tardis.mod.entity.hostile.dalek.DalekEntity;
import net.tardis.mod.items.misc.BlockGriefingLaserWeapon;
/** Allows Daleks to destroy blocks*/
public class DalekDestroyBlockGoal extends WeaponDestroyBlockGoal<DalekEntity>{
	
	public DalekDestroyBlockGoal(Class<? extends Block> blockToDestroy, DalekEntity weaponHolder, 
			BlockGriefingLaserWeapon<DalekEntity> weapon,
			double distanceToBlock, Predicate<Difficulty> difficultyPredicate) {
		super(blockToDestroy, weaponHolder, weapon,distanceToBlock, difficultyPredicate);
	}

	@Override
	public boolean shouldExecute() {
		return this.entity.getAttackTarget() != null && super.shouldExecute();
	}
	
	@Override
	public void resetTask() {
		super.resetTask();
	}

	@Override
	public boolean shouldContinueExecuting() {
		if (this.entity.getAttackTarget() != null) {
			return super.shouldContinueExecuting();    
		}
		return false;
		
	}

	@Override
	public void startExecuting() {
		super.startExecuting();
	}


}
