package net.tardis.mod.commands.argument;

import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.arguments.ArgumentType;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.DynamicCommandExceptionType;
import com.mojang.brigadier.suggestion.Suggestions;
import com.mojang.brigadier.suggestion.SuggestionsBuilder;

import net.minecraft.command.CommandSource;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.misc.Console;
import net.tardis.mod.registries.ConsoleRegistry;

public class ConsoleUnitArgument implements ArgumentType<ResourceLocation>{
	private static final Collection<String> EXAMPLES = Stream.of(ConsoleRegistry.STEAM, ConsoleRegistry.NEMO).map((console) -> {
	      return console.get().getRegistryName().toString();
	   }).collect(Collectors.toList());
    private static final DynamicCommandExceptionType INVALID_CONSOLE_EXCEPTION = new DynamicCommandExceptionType((console) -> {
      return new TranslationTextComponent("argument.tardis.console_unit.invalid", console);
    });
	
	@Override
	public ResourceLocation parse(StringReader reader) throws CommandSyntaxException {
		return ResourceLocation.read(reader);
	}
	
	@Override
	public <S> CompletableFuture<Suggestions> listSuggestions(CommandContext<S> context, SuggestionsBuilder builder) {
		Collection<ResourceLocation> consoles = ConsoleRegistry.CONSOLE_REGISTRY.get().getKeys();
		return context.getSource() instanceof ISuggestionProvider ? ISuggestionProvider.func_212476_a(consoles.stream(), builder) : Suggestions.empty();
	}

	@Override
	public Collection<String> getExamples() {
		return EXAMPLES;
	}

	public static ConsoleUnitArgument getConsoleUnitArgument() {
	      return new ConsoleUnitArgument();
	}
	
	public static Console getConsoleUnit(CommandContext<CommandSource> context, String name) throws CommandSyntaxException {
		ResourceLocation resourcelocation = context.getArgument(name, ResourceLocation.class);
		Console console = ConsoleRegistry.CONSOLE_REGISTRY.get().getValue(resourcelocation);
		if (console == null)
			throw INVALID_CONSOLE_EXCEPTION.create(resourcelocation);
		else
			return console;
	}

}
