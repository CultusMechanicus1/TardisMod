package net.tardis.mod.tileentities.machines;

import java.util.Random;

import net.minecraft.particles.ParticleTypes;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.blocks.XionCrystalBlock;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.IRift;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.ObjectWrapper;
import net.tardis.mod.particles.TParticleTypes;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.TTiles;

public class ArtronCollectorPylonTile extends TileEntity implements ITickableTileEntity{

    private ArtronCollectorTile mainUnit;
    private BlockPos crystalPos = BlockPos.ZERO;
    
    public ArtronCollectorPylonTile(TileEntityType<?> tileEntityTypeIn) {
        super(tileEntityTypeIn);
    }
    
    public ArtronCollectorPylonTile() {
        super(TTiles.ARTRON_PYLON.get());
    }

    @Override
    public void tick() {

        if(this.getOrFindMainUnit() != null) {
            this.spawnPylonParticles();
        }
        if(world.getGameTime() % 20 == 0 && !BlockPos.ZERO.equals(this.crystalPos)) {
            this.spawnXionGrowthParticles();
        }
        
        //Crystals will interrupt the artron collector charging system
        if (world.getGameTime() % 200 == 0) {
            if(BlockPos.ZERO.equals(this.crystalPos) || world.getBlockState(crystalPos).getBlock() != TBlocks.xion_crystal.get()){
                this.findNearbyCrystal();
            }
            else {
                if (world.rand.nextDouble() < 0.1D) {
                    IRift rift = world.getChunkAt(this.getPos()).getCapability(Capabilities.RIFT).orElse(null);
                    if(rift != null){
                        if(rift.getRiftEnergy() > 20){
                            rift.takeArtron(20);
                            this.spawnXionGrowthParticles();
                            XionCrystalBlock.grow(world, crystalPos);
                            world.playSound(null, this.getPos(), SoundEvents.BLOCK_FUNGUS_PLACE, SoundCategory.BLOCKS, 1F, 1F);
                        }
                    }
                    return; //Block the normal charging of artron collector if a crystal intercepts
                }
            }
        }
        
        
        if (!world.isRemote()) {
            if (world.getGameTime() % 20 == 0) { //Try to drain defined amount each second
                world.getChunkAt(getPos()).getCapability(Capabilities.RIFT).ifPresent(rift -> {
                    if(rift.isRift()) {
                        if (this.mainUnit != null) {
                            float amountToDrain = TConfig.SERVER.artronPylonRiftDrainRate.get();
                            rift.takeArtron(amountToDrain);
                            mainUnit.recieveArtron(amountToDrain);
                            world.playSound(null, this.getPos(), TSounds.ELECTRIC_SPARK.get(), SoundCategory.BLOCKS, 0.3F, 1F);
                            world.setBlockState(pos, getBlockState(), 3);
                        }
                    }
                });
            }
        }
        
    }
    
    public static Vector3d getPylonPartPos(BlockPos pos) {
        return new Vector3d(pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5);
    }

    public ArtronCollectorTile getOrFindMainUnit() {
        if(this.mainUnit != null && !this.mainUnit.isRemoved())
            return this.mainUnit;
        
        BlockPos radius = new BlockPos(16, 16, 16);
        
        ObjectWrapper<ArtronCollectorTile> mainUnitHolder = new ObjectWrapper<>(null);
        BlockPos.getAllInBoxMutable(this.getPos().subtract(radius), this.getPos().add(radius)).forEach(pos -> {
            if(pos.withinDistance(this.getPos(), 16)) {
                TileEntity te = world.getTileEntity(pos);
                if(te instanceof ArtronCollectorTile)
                    mainUnitHolder.setValue((ArtronCollectorTile)te);
            }
        });
        
        return this.mainUnit = mainUnitHolder.getValue();
        
    }

    public BlockPos findNearbyCrystal(){
        if (crystalPos != BlockPos.ZERO && world.getBlockState(crystalPos).getBlock() == TBlocks.xion_crystal.get())
            return crystalPos;
        final BlockPos radius = new BlockPos(16, 16, 16);
        for(BlockPos pos : BlockPos.getAllInBoxMutable(this.getPos().subtract(radius), this.getPos().add(radius))){
            if(this.getWorld().getBlockState(pos).getBlock() == TBlocks.xion_crystal.get()){
                crystalPos = pos.toImmutable();
                return crystalPos;
            }
        }
        return BlockPos.ZERO;
    }
    
    private void spawnPylonParticles() {
        Vector3d start = getPylonPartPos(this.mainUnit.getPos());
        Vector3d end = WorldHelper.vecFromPos(this.getPos());
        Vector3d path = start.subtract(end);
        
        Vector3d speed = path.normalize().scale(0.05);

        
        for(int i = 0; i < 10; ++i) {
            
            double percent = i / 10.0;
            
            world.addParticle(TParticleTypes.ARTRON.get(),
                    pos.getX() + 0.5 + path.getX() * percent, pos.getY() + 1.3 + path.getY() * percent, pos.getZ() + 0.5 + path.z * percent,
                    speed.x, speed.y, speed.z);
        }
        
        Random rand = world.getRandom();
        
        for(int i = 0; i < 5; ++i) {
            
            Vector3d part = new Vector3d(1.5 - rand.nextDouble() * 3, 1.5 - rand.nextDouble() * 3, 1.5 - rand.nextDouble() * 3);
            
            Vector3d partSpeed = part.subtract(new Vector3d(0.5, 0.5, 0.5)).normalize().scale(-0.05);
            
            world.addParticle(TParticleTypes.ARTRON.get(),
                    pos.getX() + 0.5 + part.getX(),pos.getY() + 0.5 + part.getY(), pos.getZ() + 0.5 + part.getZ(),
                    partSpeed.x, partSpeed.y, partSpeed.z);
        }
    }
    
    private void spawnXionGrowthParticles() {
        //Particles
        Vector3d start = getPylonPartPos(crystalPos);
        Vector3d end = WorldHelper.vecFromPos(this.getPos());
        Vector3d path = start.subtract(end);
        
        Vector3d speed = path.normalize().scale(0.05);

        
        for(int i = 0; i < 10; ++i) {
            
            double percent = i / 10.0;
            world.addParticle(TParticleTypes.ARTRON.get(),
                    pos.getX() + 0.5 + path.getX() * percent, pos.getY() + 0.2 + path.getY() * percent, pos.getZ() + 0.5 + path.z * percent,
                    speed.x, speed.y, speed.z);
        }
        
        Random rand = world.getRandom();
        for(int i = 0; i < 20; ++i){
            Vector3d part = new Vector3d(1.5 - rand.nextDouble() * 3, 1.5 - rand.nextDouble() * 3, 1.5 - rand.nextDouble() * 3);
            
            Vector3d partSpeed = part.subtract(new Vector3d(0.5, 0.5, 0.5)).normalize().scale(-0.05);
            world.addParticle(ParticleTypes.COMPOSTER, crystalPos.getX() + rand.nextDouble(), crystalPos.getY() + 0.1 + (rand.nextDouble() * 0.25), crystalPos.getZ() + rand.nextDouble(), partSpeed.x, partSpeed.y, partSpeed.z);
        }
    }
}
