package net.tardis.mod.compat.vanilla;

import net.minecraft.block.DispenserBlock;
import net.minecraft.dispenser.DefaultDispenseItemBehavior;
import net.minecraft.dispenser.IBlockSource;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnReason;
import net.minecraft.item.ItemStack;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

public class EntitySpawnerDispenseBehaviour extends DefaultDispenseItemBehavior {

    private final EntityType<?> entityType;
    private final double yOffset;
    private final double waterYOffset;
    /**
     * 
     * @param entityType
     * @param yOffset - the amount to offset the y position of the entity when spawned on top of a solid block
     * @param waterYOffset - the to offset the y position of the entity when spawned inside a water block
     */
    public EntitySpawnerDispenseBehaviour(EntityType<?> entityType, double yOffset, double waterYOffset) {
        this.entityType = entityType;
        this.yOffset = yOffset;
        this.waterYOffset = waterYOffset;
    }

    @Override
    public ItemStack dispenseStack(IBlockSource source, ItemStack stack) {
        Direction direction = source.getBlockState().get(DispenserBlock.FACING);
        World world = source.getWorld();
        Entity entity = null;
        if (!world.isRemote()) {
            ServerWorld sWorld = (ServerWorld)world;
            double d0 = source.getX() + (double) ((float) direction.getXOffset() * 1.125F);
            double d1 = source.getY() + (double) ((float) direction.getYOffset() * 1.125F);
            double d2 = source.getZ() + (double) ((float) direction.getZOffset() * 1.125F);
            BlockPos blockpos = source.getBlockPos().offset(direction);
            double d3 = 0;
            if (world.getFluidState(blockpos).isTagged(FluidTags.WATER)) {
                d3 = this.waterYOffset;
            }
            else {
            	d3 = this.yOffset;
            }
            entity = this.entityType.spawn(sWorld, stack.getTag(), (ITextComponent)null, null, blockpos, SpawnReason.DISPENSER, false, false);
            entity.rotationYaw = direction.getOpposite().getHorizontalAngle();
            entity.setPosition(d0, d1 + d3, d2);
        }
        if (entity != null) {
        	world.addEntity(entity);
            stack.shrink(1);
        }
        
        return stack;
    }
}
