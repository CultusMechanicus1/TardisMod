package net.tardis.mod.vm;


import java.util.ArrayList;
import java.util.List;

import net.minecraftforge.registries.ForgeRegistryEntry;
/**
 * Extensible Category that AbstractVortexMFunction(s) can be added to
 * @author 50ap5ud5
 *
 */
public class VortexMFunctionCategory extends ForgeRegistryEntry<VortexMFunctionCategory>{
	
	private List<AbstractVortexMFunction> VM_FUNCTIONS = new ArrayList<>();
	
	public int getListSize() {
		return VM_FUNCTIONS.size();
	}
	
	public AbstractVortexMFunction getValueFromIndex(int index) {
		return VM_FUNCTIONS.get(index);
	}
	
	public void appendFunctionToList(AbstractVortexMFunction function) {
		VM_FUNCTIONS.add(function);
	}

}
