package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.client.ClientPacketHandler;

public class LaserDamageSyncMessage {
	
	public int id = 0;
	
	public LaserDamageSyncMessage(int id) {
		this.id = id;
	}
	
	public static void encode(LaserDamageSyncMessage mes, PacketBuffer buffer) {
		buffer.writeInt(mes.id);
	}
	
	public static LaserDamageSyncMessage decode(PacketBuffer buffer) {
		return new LaserDamageSyncMessage(buffer.readInt());
	}
	
	public static void handle(LaserDamageSyncMessage mes, Supplier<NetworkEvent.Context> cont) {
		
		cont.get().enqueueWork(() -> {
			if (cont.get().getDirection() == NetworkDirection.PLAY_TO_CLIENT) {
			    ClientPacketHandler.handleLaserDamageRender(mes);
			}
		});
		
		cont.get().setPacketHandled(true);
	}

}
