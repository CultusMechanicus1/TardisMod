package net.tardis.mod.entity.hostile.dalek.weapons;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.Explosion;
import net.minecraft.world.GameRules;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.entity.hostile.dalek.DalekEntity;
import net.tardis.mod.entity.projectiles.LaserEntity;

public class DalekSpecialWeapon extends DalekWeapon{
    
    public DalekSpecialWeapon(DalekEntity dalek) {
        super(dalek);
    }

    @Override
    public void onHit(LaserEntity laserEntity, RayTraceResult result) {
        BlockPos pos = null;
        if (TConfig.SERVER.specialWeaponsAlwaysExplode.get()) { //If lasers should always explode, use the raytrace position
            pos = new BlockPos(result.getHitVec());
        }
        else {//Otherwise only give a value if its block hit
            if (result.getType() == RayTraceResult.Type.BLOCK) {
                BlockRayTraceResult blockResult = (BlockRayTraceResult) result;
                pos = blockResult.getPos();
            }
        }
        GameRules.BooleanValue shouldGrief = laserEntity.world.getGameRules().get(GameRules.MOB_GRIEFING);
        boolean shouldGriefConfig = TConfig.SERVER.dalekBlockGrief.get();
        float explosionRadius = (float)TConfig.SERVER.specialWeaponExplosionRadius.get();
        if (pos != null)
            laserEntity.world.createExplosion(laserEntity.getShooter(), laserEntity.getSource(), null, (double) pos.getX() + 0.5D, (double) pos.getY() + 0.5D, (double) pos.getZ() + 0.5D, explosionRadius, true, shouldGrief.get() && shouldGriefConfig ? Explosion.Mode.DESTROY : Explosion.Mode.NONE);
    }

}
