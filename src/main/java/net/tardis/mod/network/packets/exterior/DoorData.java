package net.tardis.mod.network.packets.exterior;

import net.minecraft.network.PacketBuffer;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class DoorData extends ExteriorData {

    EnumDoorState state;
    boolean locked;
    int deadlock;

    public DoorData(Type type) {
        super(type);
    }

    public DoorData(EnumDoorState door, boolean locked, int deadLock) {
        super(Type.DOOR);
        this.state = door;
        this.locked = locked;
        this.deadlock = deadLock;

    }

    @Override
    public void encode(PacketBuffer buf) {
        buf.writeEnumValue(this.state);
        buf.writeBoolean(this.locked);
        buf.writeInt(this.deadlock);
    }

    @Override
    public void decode(PacketBuffer buf) {
        this.state = buf.readEnumValue(EnumDoorState.class);
        this.locked = buf.readBoolean();
        this.deadlock = buf.readInt();
    }

    @Override
    public void apply(ExteriorTile tile) {
        tile.setDoorState(this.state);
        tile.setLocked(this.locked);
        tile.setAdditionalLockLevel(this.deadlock);
    }

    public static DoorData create(ExteriorTile tile){
        return new DoorData(tile.getOpen(), tile.getLocked(), tile.getAdditionalLockLevel());
    }
}
