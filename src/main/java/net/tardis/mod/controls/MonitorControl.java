package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.vector.Vector3d;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.constants.TardisConstants.Gui;
import net.tardis.mod.contexts.gui.EntityContext;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.IMonitor;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.*;
import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorView;

public class MonitorControl extends BaseControl implements IMonitor{

	private MonitorView view = MonitorView.PANORAMIC;
	
	public MonitorControl(ControlEntry entry, ConsoleTile console, ControlEntity entity) {
		super(entry, console, entity);
	}

	@Override
	public EntitySize getSize() {

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.3125F, 0.3125F);
		}
		if (getConsole() instanceof ToyotaConsoleTile) {
			return EntitySize.flexible(0.375F, 0.375F);
		}
		if(this.getConsole() instanceof XionConsoleTile)
			return EntitySize.flexible(0.3125F, 0.3125F);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return EntitySize.flexible(0.3125F, 0.3125F);
		
		return EntitySize.flexible(0.25F, 0.25F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		
		if(console.getWorld().isRemote) {
			
			if(PlayerHelper.InEitherHand(player, stack -> stack.getItem() == TItems.MONITOR_REMOTE.get())) {
				ClientHelper.openGUI(Gui.MONITOR_REMOTE, new EntityContext(this.getEntity()));
				return true;
			}
			
			if(console instanceof GalvanicConsoleTile) {
				ClientHelper.openGUI(TardisConstants.Gui.MONITOR_MAIN_GALVANIC, null);
				return true;
			}
			if (console instanceof XionConsoleTile) {
				ClientHelper.openGUI(TardisConstants.Gui.MONITOR_MAIN_XION, null);
				return true;
			}
			if (console instanceof ToyotaConsoleTile) {
				ClientHelper.openGUI(TardisConstants.Gui.MONITOR_MAIN_TOYOTA, null);
				return true;
			}
			if (console instanceof CoralConsoleTile) {
				ClientHelper.openGUI(TardisConstants.Gui.MONITOR_MAIN_CORAL, null);
				return true;
			}
			else {
				ClientHelper.openGUI(TardisConstants.Gui.MONITOR_MAIN_EYE, null);
				return true;
			}
		}
		return true;
	}

	@Override
	public Vector3d getPos() {

		if(getConsole() instanceof CoralConsoleTile) {
			return new Vector3d(0.35, 0.6875, 0.525789599508169);
		}
		if (getConsole() instanceof ToyotaConsoleTile) {
			return new Vector3d(0.534, 0.469, -0.35);
		}
		if(this.getConsole() instanceof XionConsoleTile)
			return new Vector3d(0.29892208354437577, 0.8125, 0.478818161338896);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vector3d(0.010354376833276668, 0.4375, -0.8105755249479349);
		
		return new Vector3d(-0.5282378865176893, 0.59375, -0.2954572166748212);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return null;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return null;
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putInt("view", this.view.ordinal());
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.view = MonitorView.values()[nbt.getInt("view")];
	}

	@Override
	public MonitorView getView() {
		return this.view;
	}

	@Override
	public void setView(MonitorView view) {
		this.view = view;
		this.markDirty();
	}

}
