package net.tardis.mod.datagen;

import java.nio.file.Path;

import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;

public class TardisBlockStateGen extends BlockStateProvider  {

	private final DataGenerator generator;
	
	public TardisBlockStateGen(DataGenerator gen, ExistingFileHelper exFileHelper) {
		super(gen, Tardis.MODID, exFileHelper);
		generator = gen;
	}
	
	public static Path getPath(Path path, Block block) {
		ResourceLocation key = block.getRegistryName();
		return path.resolve("assets/" + key.getNamespace() + "/blockstates/" + key.getPath() + ".json");
	}

	@Override
	protected void registerStatesAndModels() {
		for(Block block : ForgeRegistries.BLOCKS) {
			if(block.getRegistryName().getNamespace().equals(Tardis.MODID)) {
				this.simpleBlock(block);
			}
		}
	}

}
