package net.tardis.mod.tileentities.machines;

import static net.tardis.mod.blocks.RoundelTapBlock.PUSH;

import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.EnergyStorage;
import net.minecraftforge.energy.IEnergyStorage;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.ITardisWorldData;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.energy.TardisEnergy;
import net.tardis.mod.tileentities.TTiles;

/**
 * Forge Energy transferring machine that extracts energy from the Tardis dimension energy buffer and pushes it to nearby tiles
 *
 */
public class RoundelTapTile extends TileEntity implements IEnergyStorage, ITickableTileEntity{
    
    
    private IEnergyStorage energyStorage = getEnergyCap();
    // Don't create lazy optionals in getCapability. Always place them as fields in the tile entity:
    private LazyOptional<IEnergyStorage> energy = LazyOptional.of(() -> energyStorage);
    private boolean hasSetupCaps = false;

    public RoundelTapTile(TileEntityType<?> tileEntityTypeIn) {
        super(tileEntityTypeIn);
    }
    
    public RoundelTapTile() {
        super(TTiles.ROUNDEL_TAP.get());
    }

	@Override
    public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
         if (cap == CapabilityEnergy.ENERGY) {
                return energy.cast();
         }
         return super.getCapability(cap, side);
    }

	@Override
	public void remove() {
		super.remove();
	}

	@Override
    protected void invalidateCaps() {
        super.invalidateCaps();
        this.energy.invalidate(); //Properly invalidate when TE is removed or unloaded
    }

    @Override
    public int receiveEnergy(int maxReceive, boolean simulate) {
        return this.energyStorage.receiveEnergy(maxReceive, simulate);
    }

    @Override
    public int extractEnergy(int maxExtract, boolean simulate) {
        return this.energyStorage.extractEnergy(maxExtract, simulate);
    }

    @Override
    public int getEnergyStored() {
        return this.energyStorage.getEnergyStored();
    }

    @Override
    public int getMaxEnergyStored() {
        return this.energyStorage.getMaxEnergyStored();
    }

    @Override
    public boolean canExtract() {
        return this.energyStorage.canExtract();
    }

    @Override
    public boolean canReceive() {
        return this.energyStorage.canReceive();
    }
    
    private IEnergyStorage getEnergyCap() {
    	return new TardisEnergy(Integer.MAX_VALUE, TConfig.SERVER.roundelTapEnergyTransferRate.get());
    }
    
    public EnergyStorage getTardisEnergyCap() {
    	if (world != null && !world.isRemote()) {
    		ITardisWorldData cap = world.getCapability(Capabilities.TARDIS_DATA).orElse(null);
    		if(cap != null) {
    			return cap.getEnergyCap();
    		}
    		else {
        		return new EnergyStorage(0);	
        	}
    	}
    	else {
    		return new EnergyStorage(0);	
    	}
    }

    @Override
    public void tick() {
        if (!this.world.isRemote()) { //Sanity check for logical server
        	if (!this.hasSetupCaps) {
        	    this.energyStorage = this.getTardisEnergyCap();
        	    this.hasSetupCaps = true;
        	}
            this.getAndPushPower();
        }
    }
    
    public void releasePowerToTardis() {
    	if (world != null && !world.isRemote()) {
	    	world.getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> {
    		    cap.getEnergyCap().receiveEnergy(this.getEnergyStored(), false);
    	    });
    	}
    }

    public void getAndPushPower() {
    	//Push to nearby tiles if they accept Forge Energy. Otherwise add to the Tardis energy buffer
    	for(Direction dir : Direction.values()) {
            TileEntity te = world.getTileEntity(getPos().offset(dir));
            if(te != null) {
                te.getCapability(CapabilityEnergy.ENERGY, dir.getOpposite()).ifPresent(cap -> {
                	IEnergyStorage tardisEnergyCap = this.energyStorage;
                	if (this.getBlockState().hasProperty(PUSH)) {
                		//Push to nearby blocks
                		if (this.getBlockState().get(PUSH)){
                			//Get power from the Tardis
                	    	int amountToExtract = TConfig.SERVER.roundelTapEnergyTransferRate.get();
                            int simulatedReceived = cap.receiveEnergy(amountToExtract, true);
                			int received = tardisEnergyCap.extractEnergy(simulatedReceived, false);
                			cap.receiveEnergy(received, false);
                		}
                		//Push to Tardis
                		else {
                			//Push directly to Tardis capability
                			int amountToExtract = TConfig.SERVER.roundelTapEnergyTransferRate.get();
                            int simulatedReceived = tardisEnergyCap.receiveEnergy(amountToExtract, true);
                			int received = cap.extractEnergy(simulatedReceived, false);
                			tardisEnergyCap.receiveEnergy(received, false);
                		}
                	}
                });
            }
        }
    }

}
