package net.tardis.mod.world;


import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import com.mojang.datafixers.util.Pair;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.MutableRegistry;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.gen.feature.jigsaw.JigsawPattern;
import net.minecraft.world.gen.feature.jigsaw.JigsawPiece;
import net.minecraft.world.gen.feature.jigsaw.SingleJigsawPiece;
import net.minecraft.world.raid.Raid;
import net.minecraftforge.fml.event.server.FMLServerAboutToStartEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.helper.Helper;

public class WorldGen {
    
    public static List<BlockPos> BROKEN_EXTERIORS = new ArrayList<>();
    
    /**
     * <p> Adds the building to the targeted pool.
     * <br> We will call this in addStructuresToJigsawPools method to add to every village.
     *
     * <br> Note: This is an additive operation which means multiple mods can do this and they stack with each other safely.
     * <br> Based off https://gist.github.com/TelepathicGrunt/4fdbc445ebcbcbeb43ac748f4b18f342
     */
    private static void addStructureToTemplatePool(MutableRegistry<JigsawPattern> templatePoolRegistry, ResourceLocation poolRL, String nbtPieceRL, int weight) {

        // Grab the pool we want to add to
        JigsawPattern pool = templatePoolRegistry.getOrDefault(poolRL);
        if (pool == null) return;

        // Grabs the nbt piece and creates a SingleJigsawPiece of it that we can add to a structure's pool.
        //NOTE: SingleJigsawPiece requires the structure to have Structure Void blocks inside the structure which replaces every air block
        //If we want to make Air act as Structure Voids we can use Legacy SingleJigsawPiece. 
        //However keep in mind the legacy type will not replace real Structure voids with terrain, so each approach as their trade offs.
        SingleJigsawPiece piece = SingleJigsawPiece.func_242859_b(nbtPieceRL).apply(JigsawPattern.PlacementBehaviour.RIGID);

        // AccessTransformer to make JigsawPattern's jigsawPieces field public for us to see.
        // public net.minecraft.world.gen.feature.jigsaw.JigsawPattern field_214953_e #jigsawPieces
        // Weight is handled by how many times the entry appears in this list.
        // We do not need to worry about immutability as this field is created using Lists.newArrayList(); which makes a mutable list.
        for (int i = 0; i < weight; i++) {
            pool.jigsawPieces.add(piece);
        }

        // AccessTransformer to make JigsawPattern's rawTemplates field public and non-final for us to see.
        // public-f net.minecraft.world.gen.feature.jigsaw.JigsawPattern field_214952_d #rawTemplates
        // This list of pairs of pieces and weights is not used by vanilla by default but another mod may need it for efficiency.
        // So lets add to this list for completeness. We need to make a copy of the array as it can be an immutable list.
        List<Pair<JigsawPiece, Integer>> listOfPieceEntries = new ArrayList<>(pool.rawTemplates);
        listOfPieceEntries.add(new Pair<>(piece, weight));
        pool.rawTemplates = listOfPieceEntries;
    }
    
    /** Add the street structure that spawns the Observatory structure, into a template pool
     * <br> This street structure is filled with Structure Void Blocks to allow terrain to be preserved since we are using SingleJigsawPiece*/
    private static void addObservatoryStreetToPools(MutableRegistry<JigsawPattern> templatePoolRegistry, String biome, int weight) {
        addStructureToTemplatePool(templatePoolRegistry, new ResourceLocation("minecraft:village/" + biome + "/streets"), new ResourceLocation(Tardis.MODID, "village/" + biome + "/streets/" + "observatory_street_" + biome).toString(), weight);
    }
    
    /**
     * Add all Structures to existing Jigsaw Pools
     * <br> Call in FMLServerAboutToStartEvent as the dynamic registry exists now and all JSON worldgen files were parsed.
     */
    public static void addStructuresToJigsawPools(final FMLServerAboutToStartEvent event) {
        MutableRegistry<JigsawPattern> templatePoolRegistry = event.getServer().getDynamicRegistries().getRegistry(Registry.JIGSAW_POOL_KEY);
        //Add Village version of Observatory Structure to following village types
        //This is using a special jigsaw compatible version of our structures
        addObservatoryStreetToPools(templatePoolRegistry, "plains", TConfig.COMMON.observatorySpawnWeightPlains.get());
        addObservatoryStreetToPools(templatePoolRegistry, "desert", TConfig.COMMON.observatorySpawnWeightDesert.get());
        addObservatoryStreetToPools(templatePoolRegistry, "savanna", TConfig.COMMON.observatorySpawnWeightSavanna.get());
    }

    @Nullable
    public static BlockPos getClosestBrokenExterior(int x, int y) {
        int dist = Integer.MAX_VALUE;
        BlockPos ext = null;
        for (BlockPos pos : BROKEN_EXTERIORS) {
            if (pos.distanceSq(new BlockPos(x, 0, y)) < dist)
                ext = pos;

        }
        if (BROKEN_EXTERIORS.contains(ext))
            BROKEN_EXTERIORS.remove(ext);
        return ext;
    }
    
    public static void addEntitiesToRaids() {
        if (TConfig.COMMON.dalekJoinVillageRaid.get()) {
            //Add Daleks to raid waves
            int[] dalekRaidCount = Helper.toIntArray(TConfig.COMMON.dalekVillageRaidSpawnCount.get());
            Raid.WaveMember.create(new ResourceLocation(Tardis.MODID,"dalek_raiders").toString(), TEntities.DALEK.get(), dalekRaidCount);
        }
    }
}
