package net.tardis.mod.subsystem;

import net.minecraft.entity.AreaEffectCloudEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Potion;
import net.tardis.mod.misc.ITickable;
import net.tardis.mod.potions.TardisPotions;
import net.tardis.mod.tileentities.ConsoleTile;

public class FluidLinksSubsystem extends Subsystem implements ITickable{

	public FluidLinksSubsystem(ConsoleTile console, Item item) {
		super(console, item);
		console.registerTicker(this);
	}

	@Override
	public CompoundNBT serializeNBT() {
		return super.serializeNBT();
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		super.deserializeNBT(nbt);
	}


	@Override
    public void tick(ConsoleTile console) {}

    @Override
    public void onTakeoff() {
        this.damage((ServerPlayerEntity)this.console.getPilot(), 1);
    }

    @Override
    public void onLand() {
        this.damage((ServerPlayerEntity)this.console.getPilot(), 1);
    }

    @Override
    public void onFlightSecond() {}
    
    @Override
	public void onBreak() {
		super.onBreak();
		if(console != null && !console.getWorld().isRemote) {
			AreaEffectCloudEntity potion = new AreaEffectCloudEntity(console.getWorld(), console.getPos().getX() + 0.5, console.getPos().getY(), console.getPos().getZ() + 0.5);
			potion.setDuration(30 * 20);
			potion.setRadius(16);
			potion.setPotion(new Potion(new EffectInstance(TardisPotions.MERCURY.get(), 30 * 20, 0)));
			console.getWorld().addEntity(potion);
		}
	}

}
