package net.tardis.mod.tileentities;

import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.misc.SpaceTimeCoord;

public interface IAffectTARDISLanding {

    /**
     *
     * @param world
     * @param currentLanding
     * @param console
     * @return - Null if unaffected, new space time coord otherwise
     */
    SpaceTimeCoord affectTARDIS(ServerWorld world, SpaceTimeCoord currentLanding, ConsoleTile console);
    int getEffectiveRange();
}
