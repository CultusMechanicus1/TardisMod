package net.tardis.mod.missions.stages;

import java.util.function.Supplier;

import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;

public class MissionStages {
	public static final DeferredRegister<MissionStage> STAGES = DeferredRegister.create(MissionStage.class, Tardis.MODID);
	
	public static Supplier<IForgeRegistry<MissionStage>> MISSION_STAGES_REGISTRY = STAGES.makeRegistry("mission_stage", () -> new RegistryBuilder<MissionStage>().setMaxID(Integer.MAX_VALUE - 1));
	
	public static final RegistryObject<MissionStage> NOT_STARTED = STAGES.register("not_started", () -> new MissionStage(1));
	public static final RegistryObject<MissionStage> RETURN_TO_MISSION_HOST = STAGES.register("return_to_mission_host", () -> new MissionStage(1));
	public static final RegistryObject<MissionStage> MISSION_COMPLETED = STAGES.register("mission_completed", () -> new MissionStage(1));

	public static final RegistryObject<MissionStage> KILL_DRONES = STAGES.register("kill_drones", () -> new DroneKillMissionStage());
	

}
