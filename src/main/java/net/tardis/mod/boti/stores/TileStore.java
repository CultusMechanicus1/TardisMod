package net.tardis.mod.boti.stores;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistries;

public class TileStore {

    public BlockPos pos;
    public CompoundNBT tag;

    public TileStore(BlockPos pos, CompoundNBT tag){
        this.pos = pos;
        this.tag = tag;
    }

    public TileStore(BlockPos pos, TileEntity te){
        CompoundNBT tag = te.serializeNBT();
        this.pos = pos.toImmutable();
        this.tag = tag;
    }

    public ResourceLocation getRegistryKey(){
        return new ResourceLocation(this.tag.getString("id"));
    }

    public TileEntity createTile(World world){
        TileEntity te = ForgeRegistries.TILE_ENTITIES.getValue(this.getRegistryKey()).create();
        if(te != null) {
            te.deserializeNBT(this.tag);
            te.setWorldAndPos(world, this.pos);
        }
        return te;
    }

    public void encode(PacketBuffer buf){
        buf.writeBlockPos(this.pos);
        buf.writeCompoundTag(this.tag);
    }

    public static TileStore decode(PacketBuffer buf){
        return new TileStore(buf.readBlockPos(), buf.readCompoundTag());
    }

}
