package net.tardis.mod.schematics;

import java.io.InputStreamReader;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import org.apache.logging.log4j.Level;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import net.minecraft.client.resources.ReloadListener;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.profiler.IProfiler;
import net.minecraft.resources.IFutureReloadListener;
import net.minecraft.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.fml.network.simple.SimpleChannel;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.tardis.mod.Tardis;
import net.tardis.mod.registries.SchematicTypes;
import net.tardis.mod.schematics.types.SchematicType;

public class SchematicDataListener extends ReloadListener<Map<ResourceLocation, Schematic>> {

    public static final SchematicDataListener INSTANCE = new SchematicDataListener();
    protected static final String folderName = "unlock_schematics";

    protected Optional<Runnable> syncDataOnReload = Optional.empty();

    public static void read(IResourceManager manager){

        Schematics.SCHEMATIC_REGISTRY.clear();
        Tardis.LOGGER.info("Beginning loading of data for data loader: {}", folderName);
        for(ResourceLocation loc : manager.getAllResourceLocations(folderName, str -> str.endsWith(".json"))){
            
            try{

                JsonObject root = new JsonParser().parse(new InputStreamReader(manager.getResource(loc).getInputStream())).getAsJsonObject();

                ResourceLocation type = new ResourceLocation(root.get("type").getAsString());

                SchematicType schematicType = SchematicTypes.REGISTRY.get().getValue(type);

                if(schematicType != null){

                    ResourceLocation key = stripResourceLocation(loc);

                    Schematic schematic = schematicType.deserialize(root);
                    schematic.setId(key);
                    Schematics.SCHEMATIC_REGISTRY.put(key, schematic);
                    Tardis.LOGGER.info("Added schematic: {}", key);

                }
                else Tardis.LOGGER.log(Level.ERROR, String.format("No SchematicType called %s!", type));

            }
            catch(Exception e){
                Tardis.LOGGER.log(Level.ERROR, String.format("Invalid json for schematic %s!", loc));
                Tardis.LOGGER.catching(Level.ERROR, e);
            }

        }
        Tardis.LOGGER.info("Data loader for {} loaded {} entries", folderName, Schematics.SCHEMATIC_REGISTRY.size());

    }

    public static ResourceLocation stripResourceLocation(ResourceLocation loc){
        return new ResourceLocation(loc.getNamespace(), loc.getPath().replace(folderName + "/", "").replace(".json", ""));
    }

    @Override
    protected Map<ResourceLocation, Schematic> prepare(IResourceManager resourceManager, IProfiler profilerIn) {
        SchematicDataListener.read(resourceManager);
        Map<ResourceLocation, Schematic> map = Schematics.SCHEMATIC_REGISTRY;
        return map;
    }

    @Override
    protected void apply(Map<ResourceLocation, Schematic> objects, IResourceManager resourceManagerIn,
            IProfiler profilerIn) {
        //Sync the data to the player if the server is ready
        if (ServerLifecycleHooks.getCurrentServer() != null) {
            // if we're on the server and we are configured to send syncing packets, send syncing packets
            this.syncDataOnReload.ifPresent(Runnable::run);
        }
    }
    
    public <PACKET> IFutureReloadListener subscribeAsSyncable(final SimpleChannel channel,
        final Function<Map<ResourceLocation, Schematic>, PACKET> packetFactory){
        MinecraftForge.EVENT_BUS.addListener(this.getLoginListener(channel, packetFactory));
        this.syncDataOnReload = Optional.of(() -> channel.send(PacketDistributor.ALL.noArg(), packetFactory.apply(Schematics.SCHEMATIC_REGISTRY)));
        return this;
    }
    
    private <PACKET> Consumer<PlayerEvent.PlayerLoggedInEvent> getLoginListener(final SimpleChannel channel,
            final Function<Map<ResourceLocation, Schematic>, PACKET> packetFactory){
        return event -> {
            PlayerEntity player = event.getPlayer();
            if (player instanceof ServerPlayerEntity){
                channel.send(PacketDistributor.PLAYER.with(() -> (ServerPlayerEntity)player), packetFactory.apply(Schematics.SCHEMATIC_REGISTRY));
            }
        };
    }
}
