package net.tardis.api.events;

import net.minecraft.entity.Entity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.EntityEvent;

/**
 * <p>Allows modders to play a {@code Sound} on the server.</p>
 *
 * <p>This event fired in {@link ServerWorldMixin#playSound}, on the
 * {@link net.minecraftforge.common.MinecraftForge#EVENT_BUS} only on the
 * {@linkplain net.minecraftforge.fml.LogicalSide#SERVER server-side}.
 */
@net.minecraftforge.eventbus.api.Cancelable
public class ServerSoundEvent extends EntityEvent {
    private final World world;
    private SoundEvent name;
    private SoundCategory category;
    private final float volume;
    private final float pitch;
    private float newVolume;
    private float newPitch;
    private BlockPos blockPos;

    public ServerSoundEvent(World world, Entity entity, SoundEvent name, SoundCategory category, float volume, float pitch, BlockPos blockPos) {
        super(entity);
        this.name = name;
        this.category = category;
        this.volume = volume;
        this.pitch = pitch;
        this.newVolume = volume;
        this.newPitch = pitch;
        this.blockPos = blockPos;
        this.world = world;
    }

    public SoundEvent getSound() {
        return this.name;
    }

    public SoundCategory getCategory() {
        return this.category;
    }

    public float getDefaultVolume() {
        return this.volume;
    }

    public float getDefaultPitch() {
        return this.pitch;
    }

    public float getVolume() {
        return this.newVolume;
    }

    public float getPitch() {
        return this.newPitch;
    }

    public void setSound(SoundEvent value) {
        this.name = value;
    }

    public void setCategory(SoundCategory category) {
        this.category = category;
    }

    public void setVolume(float value) {
        this.newVolume = value;
    }

    public void setPitch(float value) {
        this.newPitch = value;
    }

    public BlockPos getBlockPos() {
        return blockPos;
    }

    public World getWorld() {
        return world;
    }
}
