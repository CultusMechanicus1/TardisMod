package net.tardis.mod.misc;

import net.minecraft.util.SoundEvent;
import net.tardis.mod.sounds.TSounds;

public enum DoorSounds implements IDoorSoundScheme {

    BASE(TSounds.DOOR_OPEN.get(), TSounds.DOOR_CLOSE.get()),
    WOOD(TSounds.WOOD_DOOR_OPEN.get(), TSounds.WOOD_DOOR_CLOSE.get());

    final SoundEvent open;
    final SoundEvent closed;

    DoorSounds(SoundEvent open, SoundEvent closed) {
        this.open = open;
        this.closed = closed;
    }

    @Override
    public SoundEvent getOpenSound() {
        return this.open;
    }

    @Override
    public SoundEvent getClosedSound() {
        return this.closed;
    }
}
