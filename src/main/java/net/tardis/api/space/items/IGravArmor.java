package net.tardis.api.space.items;

import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
/** Interface to allow armor items to allow the player to remain on the ground when the environment has no gravity*/
public interface IGravArmor {

    boolean useNormalGrav(LivingEntity entity, ItemStack stack);
}
