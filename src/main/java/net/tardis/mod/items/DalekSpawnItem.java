package net.tardis.mod.items;

import java.util.Objects;

import com.google.common.base.Supplier;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.FlowingFluidBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.stats.Stats;
import net.minecraft.tileentity.MobSpawnerTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.spawner.AbstractSpawner;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.hostile.dalek.DalekEntity;
import net.tardis.mod.entity.hostile.dalek.types.DalekType;
import net.tardis.mod.itemgroups.TItemGroups;


public class DalekSpawnItem extends Item {

    Supplier<DalekType> dalekType;

    public DalekSpawnItem(Supplier<DalekType> dalekType) {
        super(new Item.Properties().group(TItemGroups.MAIN));
        this.dalekType = dalekType;
    }

    @Override
    public ActionResultType onItemUse(ItemUseContext context) {
        World world = context.getWorld();
        if (world.isRemote()) {
           return ActionResultType.SUCCESS;
        } else {
           ItemStack itemstack = context.getItem();
           BlockPos blockpos = context.getPos();
           Direction direction = context.getFace();
           BlockState blockstate = world.getBlockState(blockpos);
           EntityType<DalekEntity> dalekType = TEntities.DALEK.get();
           if (blockstate.matchesBlock(Blocks.SPAWNER)) {
              TileEntity tileentity = world.getTileEntity(blockpos);
              if (tileentity instanceof MobSpawnerTileEntity) {
                 AbstractSpawner abstractspawner = ((MobSpawnerTileEntity)tileentity).getSpawnerBaseLogic();
                 abstractspawner.setEntityType(dalekType);
                 tileentity.markDirty();
                 world.notifyBlockUpdate(blockpos, blockstate, blockstate, 3);
                 itemstack.shrink(1);
                 return ActionResultType.CONSUME;
              }
           }

           BlockPos testPos;
           if (blockstate.getCollisionShapeUncached(world, blockpos).isEmpty()) {
              testPos = blockpos;
           } else {
              testPos = blockpos.offset(direction);
           }
           Entity entity = dalekType.spawn((ServerWorld)world, itemstack, context.getPlayer(), testPos, SpawnReason.SPAWN_EGG, true, !Objects.equals(blockpos, testPos) && direction == Direction.UP);
           if (entity != null && entity instanceof DalekEntity) {
              DalekEntity dalek = (DalekEntity)entity;
              dalek.setDalekType(this.dalekType.get());
              itemstack.shrink(1);
           }

           return ActionResultType.CONSUME;
        }
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
        ItemStack itemstack = playerIn.getHeldItem(handIn);
        RayTraceResult raytraceresult = Item.rayTrace(worldIn, playerIn, RayTraceContext.FluidMode.SOURCE_ONLY);
        if (worldIn.isRemote()) {
           return ActionResult.resultSuccess(itemstack);
        }
        if (raytraceresult.getType() != RayTraceResult.Type.BLOCK) {
            return ActionResult.resultPass(itemstack);
        }
        else {
           BlockRayTraceResult blockraytraceresult = (BlockRayTraceResult)raytraceresult;
           BlockPos blockpos = blockraytraceresult.getPos();
           if (!(worldIn.getBlockState(blockpos).getBlock() instanceof FlowingFluidBlock)) {
              return ActionResult.resultPass(itemstack);
           } 
           else if (worldIn.isBlockModifiable(playerIn, blockpos) && playerIn.canPlayerEdit(blockpos, blockraytraceresult.getFace(), itemstack)) {
               EntityType<DalekEntity> dalekType = TEntities.DALEK.get();
               Entity entity = dalekType.spawn((ServerWorld)worldIn, itemstack, playerIn, blockpos, SpawnReason.SPAWN_EGG, false, false);
               if (entity != null && entity instanceof DalekEntity) {
                   DalekEntity dalek = (DalekEntity)entity;
                   dalek.setDalekType(this.dalekType.get());
                   if (!playerIn.abilities.isCreativeMode) {
                       itemstack.shrink(1);
                       playerIn.addStat(Stats.ITEM_USED.get(this));
                       return ActionResult.resultSuccess(itemstack);
                    }
               }
           } else {
              return ActionResult.resultFail(itemstack);
           }
           return ActionResult.resultFail(itemstack);
      }
   }

}
