package net.tardis.mod.client.guis.widgets;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;

public class TabWidget extends Button {

    public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/widgets/tabs.png");
    public boolean isSelected;
    private ItemStack item;
    
    public TabWidget(int xIn, int yIn, ItemStack stack, TranslationTextComponent title, IPressable press) {
        super(xIn, yIn, 35, 31, title, press);
        this.item = stack;
    }
    
    public TabWidget(int xIn, int yIn, ItemStack stack, ITextComponent title, IPressable press) {
        super(xIn, yIn, 35, 31, new TranslationTextComponent(""), press);
        this.item = stack;
    }
    
    //TODO: Convert to IVertexBuffers as per 1.16 standards
    @Override
    public void renderWidget(MatrixStack matrixStack, int x, int y, float partialTicks) {
    	matrixStack.push();
        RenderSystem.enableAlphaTest();
        RenderSystem.enableBlend();
        RenderHelper.enableStandardItemLighting();
        Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
        if (this.isSelected)
            blit(matrixStack, x, y, 0, 34, 34, 66);
        else blit(matrixStack, x, y, 0, 0, 43, 31);
        Minecraft.getInstance().getItemRenderer().renderItemAndEffectIntoGUI(item, x + 8, y + 8);
        RenderSystem.disableAlphaTest();
        RenderHelper.disableStandardItemLighting();
        RenderSystem.disableBlend();
        matrixStack.pop();
    }

}