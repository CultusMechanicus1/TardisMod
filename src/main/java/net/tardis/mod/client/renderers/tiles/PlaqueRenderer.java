package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextProcessing;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.PlaqueTile;

public class PlaqueRenderer extends TileEntityRenderer<PlaqueTile> {

	public static WorldText TEXT = new WorldText(0.9F, 0.335F, 0.009F, 0xFFFFFF);
	
	public PlaqueRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	@Override
	public void render(PlaqueTile plaque, float partialTicks, MatrixStack matrixStackIn,
	        IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
		matrixStackIn.push();
		matrixStackIn.translate(0.5, 0, 0.5);
		matrixStackIn.rotate(Vector3f.ZP.rotationDegrees(180));
		matrixStackIn.rotate(Vector3f.YP.rotationDegrees(WorldHelper.getAngleFromFacing(plaque.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING))));
		matrixStackIn.translate(-0.45, -0.62, 6.8 / 16.0);
		TEXT.renderText(matrixStackIn, bufferIn, combinedLightIn, plaque.getText());
		matrixStackIn.pop();
	}
	
	public void debugTextAndBotiConflict(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
		FontRenderer fr = Minecraft.getInstance().fontRenderer;
		float scale = 0.009F;
		matrixStackIn.scale(scale, scale, scale);
		int x = 0;
		int y = 0;
		String text = "hi";
		int color = 0xFFFFFF;
		Matrix4f matrix = matrixStackIn.getLast().getMatrix();
		boolean dropShadow = true;
		Vector3f FONT_OFFSET = new Vector3f(0.0F, 0.0F, 0.03F);

	}
	
	
	

}
