package net.tardis.mod.schematics.types;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.schematics.ConsoleUnlockSchematic;
import net.tardis.mod.schematics.Schematic;

public class ConsoleSchematicType extends SchematicType{

    @Override
    public Schematic deserialize(JsonObject root) {

        ConsoleUnlockSchematic schematic = new ConsoleUnlockSchematic(this);
        schematic.setConsole(new ResourceLocation(root.get("result").getAsString()));
        setTranslationOrDisplayName(root, schematic);
        return schematic;
    }
    
    @Override
    public Schematic deserialize(PacketBuffer buffer) {
        ConsoleUnlockSchematic schematic = new ConsoleUnlockSchematic(this);
        schematic.setConsole(buffer.readResourceLocation());
        return schematic;
    }


    @Override
    public JsonObject serialize(Schematic schematic) {

        ConsoleUnlockSchematic console = (ConsoleUnlockSchematic)schematic;

        JsonObject root = this.createBaseJson(schematic.getDisplayName(), schematic.isUsingTranslatedName());
        root.add("result", new JsonPrimitive(console.getConsole().toString()));
        return root;
    }

    @Override
    public Schematic serialize(Schematic schematic, PacketBuffer buffer) {
        ConsoleUnlockSchematic schem = this.getSchematicAs(ConsoleUnlockSchematic.class, schematic);
        buffer.writeResourceLocation(schem.getConsole());
        return schem;
    }

}
