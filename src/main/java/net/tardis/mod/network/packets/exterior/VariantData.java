package net.tardis.mod.network.packets.exterior;

import net.minecraft.network.PacketBuffer;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class VariantData extends ExteriorData{

    private int variant;

    public VariantData(Type type) {
        super(type);
    }

    public VariantData(int variant){
        this(Type.VARIANT);
        this.variant = variant;
    }

    @Override
    public void encode(PacketBuffer buf) {
        buf.writeInt(this.variant);
    }

    @Override
    public void decode(PacketBuffer buf) {
        this.variant = buf.readInt();
    }

    @Override
    public void apply(ExteriorTile object) {
        object.setVariant(this.variant);
    }
}
