package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.network.TPacketHandler;


public class VMTeleportMessage {

	public BlockPos pos;
	public boolean teleportPrecise;
	private boolean dimensionTeleport;
	public RegistryKey<World> worldKey;
	
	public VMTeleportMessage(BlockPos pos, RegistryKey<World> worldKey, boolean teleportPrecise, boolean dimensionTeleport) {
		this.pos = pos;
		this.worldKey = worldKey;
		this.teleportPrecise = teleportPrecise;
		this.dimensionTeleport = dimensionTeleport;
	}


	public static void encode(VMTeleportMessage mes,PacketBuffer buf) {
		buf.writeBlockPos(mes.pos);
		buf.writeResourceLocation(mes.worldKey.getLocation());
		buf.writeBoolean(mes.teleportPrecise);
		buf.writeBoolean(mes.dimensionTeleport);
	}
	
	public static VMTeleportMessage decode(PacketBuffer buf) {
		return new VMTeleportMessage(buf.readBlockPos(), RegistryKey.getOrCreateKey(Registry.WORLD_KEY, buf.readResourceLocation()),buf.readBoolean(), buf.readBoolean());
	}
	
    public static void handle(VMTeleportMessage mes,  Supplier<NetworkEvent.Context> ctx){
        ctx.get().enqueueWork(()->{
    	    ServerPlayerEntity sender = ctx.get().getSender();
    	    if (mes.dimensionTeleport)
    	        TPacketHandler.handleVortexMTeleport(sender, mes.pos, mes.worldKey, true, mes.teleportPrecise);
    	    else TPacketHandler.handleVortexMTeleport(sender, mes.pos, mes.teleportPrecise);
        });
        ctx.get().setPacketHandled(true);
    }
    
}
