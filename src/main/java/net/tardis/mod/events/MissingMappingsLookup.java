package net.tardis.mod.events;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class MissingMappingsLookup {
	
	public static JsonObject getMissingMappings() {
		JsonObject missingMappings = new JsonObject();
		
		//Remap Roundels
	    missingMappings.add("roundel_wood_oak", new JsonPrimitive("roundel/oak_planks_full"));
		missingMappings.add("roundel_offset_wood_oak", new JsonPrimitive("roundel/oak_planks_offset"));
		missingMappings.add("roundel_wood_spruce", new JsonPrimitive("roundel/spruce_planks_full"));
		missingMappings.add("roundel_offset_wood_spruce", new JsonPrimitive("roundel/spruce_planks_offset"));
		missingMappings.add("roundel_wood_birch", new JsonPrimitive("roundel/birch_planks_full"));
		missingMappings.add("roundel_offset_wood_birch", new JsonPrimitive("roundel/birch_planks_offset"));
		missingMappings.add("roundel_wood_jungle", new JsonPrimitive("roundel/jungle_planks_full"));
		missingMappings.add("roundel_offset_wood_jungle", new JsonPrimitive("roundel/jungle_planks_offset"));
		missingMappings.add("roundel_wood_acacia", new JsonPrimitive("roundel/acacia_planks_full"));
		missingMappings.add("roundel_offset_wood_acacia", new JsonPrimitive("roundel/acacia_planks_offset"));
		missingMappings.add("roundel_wood_dark_oak", new JsonPrimitive("roundel/dark_oak_planks_full"));
		missingMappings.add("roundel_offset_wood_dark_oak", new JsonPrimitive("roundel/dark_oak_planks_offset"));

		missingMappings.add("roundel_concrete_white", new JsonPrimitive("roundel/white_concrete_full"));
		missingMappings.add("roundel_offset_concrete_white", new JsonPrimitive("roundel/white_concrete_offset"));
		missingMappings.add("roundel_concrete_orange", new JsonPrimitive("roundel/orange_concrete_full"));
		missingMappings.add("roundel_offset_concrete_orange", new JsonPrimitive("roundel/orange_concrete_offset"));
		missingMappings.add("roundel_concrete_magenta", new JsonPrimitive("roundel/magenta_concrete_full"));
		missingMappings.add("roundel_offset_concrete_magenta", new JsonPrimitive("roundel/magenta_concrete_offset"));
		missingMappings.add("roundel_concrete_light_blue", new JsonPrimitive("roundel/light_blue_concrete_full"));
		missingMappings.add("roundel_offset_concrete_light_blue", new JsonPrimitive("roundel/light_blue_concrete_offset"));
		missingMappings.add("roundel_concrete_yellow", new JsonPrimitive("roundel/yellow_concrete_full"));
		missingMappings.add("roundel_offset_concrete_yellow", new JsonPrimitive("roundel/yellow_concrete_offset"));
		missingMappings.add("roundel_concrete_lime", new JsonPrimitive("roundel/lime_concrete_full"));
		missingMappings.add("roundel_offset_concrete_lime", new JsonPrimitive("roundel/lime_concrete_offset"));
		missingMappings.add("roundel_concrete_pink", new JsonPrimitive("roundel/pink_concrete_full"));
		missingMappings.add("roundel_offset_concrete_pink", new JsonPrimitive("roundel/pink_concrete_offset"));
		missingMappings.add("roundel_concrete_gray", new JsonPrimitive("roundel/gray_concrete_full"));
		missingMappings.add("roundel_offset_concrete_gray", new JsonPrimitive("roundel/gray_concrete_offset"));
		missingMappings.add("roundel_concrete_light_gray", new JsonPrimitive("roundel/light_gray_concrete_full"));
		missingMappings.add("roundel_offset_concrete_light_gray", new JsonPrimitive("roundel/light_gray_concrete_offset"));
		missingMappings.add("roundel_concrete_cyan", new JsonPrimitive("roundel/cyan_concrete_full"));
		missingMappings.add("roundel_offset_concrete_cyan", new JsonPrimitive("roundel/cyan_concrete_offset"));
		missingMappings.add("roundel_concrete_purple", new JsonPrimitive("roundel/purple_concrete_full"));
		missingMappings.add("roundel_offset_concrete_purple", new JsonPrimitive("roundel/purple_concrete_offset"));
		missingMappings.add("roundel_concrete_blue", new JsonPrimitive("roundel/blue_concrete_full"));
		missingMappings.add("roundel_offset_concrete_blue", new JsonPrimitive("roundel/blue_concrete_offset"));
		missingMappings.add("roundel_concrete_brown", new JsonPrimitive("roundel/brown_concrete_full"));
		missingMappings.add("roundel_offset_concrete_brown", new JsonPrimitive("roundel/brown_concrete_offset"));
		missingMappings.add("roundel_concrete_green", new JsonPrimitive("roundel/green_concrete_full"));
		missingMappings.add("roundel_offset_concrete_green", new JsonPrimitive("roundel/green_concrete_offset"));
		missingMappings.add("roundel_concrete_red", new JsonPrimitive("roundel/red_concrete_full"));
		missingMappings.add("roundel_offset_concrete_red", new JsonPrimitive("roundel/red_concrete_offset"));
		missingMappings.add("roundel_concrete_black", new JsonPrimitive("roundel/black_concrete_full"));
		missingMappings.add("roundel_offset_concrete_black", new JsonPrimitive("roundel/black_concrete_offset"));

		missingMappings.add("roundel_terracotta_white", new JsonPrimitive("roundel/white_terracotta_full"));
		missingMappings.add("roundel_offset_terracotta_white", new JsonPrimitive("roundel/white_terracotta_offset"));
		missingMappings.add("roundel_terracotta_orange", new JsonPrimitive("roundel/orange_terracotta_full"));
		missingMappings.add("roundel_offset_terracotta_orange", new JsonPrimitive("roundel/orange_terracotta_offset"));
		missingMappings.add("roundel_terracotta_magenta", new JsonPrimitive("roundel/magenta_terracotta_full"));
		missingMappings.add("roundel_offset_terracotta_magenta", new JsonPrimitive("roundel/magenta_terracotta_offset"));
		missingMappings.add("roundel_terracotta_light_blue", new JsonPrimitive("roundel/light_blue_terracotta_full"));
		missingMappings.add("roundel_offset_terracotta_light_blue", new JsonPrimitive("roundel/light_blue_terracotta_offset"));
		missingMappings.add("roundel_terracotta_yellow", new JsonPrimitive("roundel/yellow_terracotta_full"));
		missingMappings.add("roundel_offset_terracotta_yellow", new JsonPrimitive("roundel/yellow_terracotta_offset"));
		missingMappings.add("roundel_terracotta_lime", new JsonPrimitive("roundel/lime_terracotta_full"));
		missingMappings.add("roundel_offset_terracotta_lime", new JsonPrimitive("roundel/lime_terracotta_offset"));
		missingMappings.add("roundel_terracotta_pink", new JsonPrimitive("roundel/pink_terracotta_full"));
		missingMappings.add("roundel_offset_terracotta_pink", new JsonPrimitive("roundel/pink_terracotta_offset"));
		missingMappings.add("roundel_terracotta_gray", new JsonPrimitive("roundel/gray_terracotta_full"));
		missingMappings.add("roundel_offset_terracotta_gray", new JsonPrimitive("roundel/gray_terracotta_offset"));
		missingMappings.add("roundel_terracotta_light_gray", new JsonPrimitive("roundel/light_gray_terracotta_full"));
		missingMappings.add("roundel_offset_terracotta_light_gray", new JsonPrimitive("roundel/light_gray_terracotta_offset"));
		missingMappings.add("roundel_terracotta_cyan", new JsonPrimitive("roundel/cyan_terracotta_full"));
		missingMappings.add("roundel_offset_terracotta_cyan", new JsonPrimitive("roundel/cyan_terracotta_offset"));
		missingMappings.add("roundel_terracotta_purple", new JsonPrimitive("roundel/purple_terracotta_full"));
		missingMappings.add("roundel_offset_terracotta_purple", new JsonPrimitive("roundel/purple_terracotta_offset"));
		missingMappings.add("roundel_terracotta_blue", new JsonPrimitive("roundel/blue_terracotta_full"));
		missingMappings.add("roundel_offset_terracotta_blue", new JsonPrimitive("roundel/blue_terracotta_offset"));
		missingMappings.add("roundel_terracotta_brown", new JsonPrimitive("roundel/brown_terracotta_full"));
		missingMappings.add("roundel_offset_terracotta_brown", new JsonPrimitive("roundel/brown_terracotta_offset"));
		missingMappings.add("roundel_terracotta_green", new JsonPrimitive("roundel/green_terracotta_full"));
		missingMappings.add("roundel_offset_terracotta_green", new JsonPrimitive("roundel/green_terracotta_offset"));
		missingMappings.add("roundel_terracotta_red", new JsonPrimitive("roundel/red_terracotta_full"));
		missingMappings.add("roundel_offset_terracotta_red", new JsonPrimitive("roundel/red_terracotta_offset"));  
		missingMappings.add("roundel_terracotta_black", new JsonPrimitive("roundel/black_terracotta_full"));
		missingMappings.add("roundel_offset_terracotta_black", new JsonPrimitive("roundel/black_terracotta_offset"));

		missingMappings.add("roundel_quartz_pillar", new JsonPrimitive("roundel/quartz_pillar_full"));
		missingMappings.add("roundel_quartz", new JsonPrimitive("roundel/quartz"));
		missingMappings.add("roundel_chiselled_quartz", new JsonPrimitive("roundel/chiseled_quartz_full"));
		
		missingMappings.add("roundel_alabaster", new JsonPrimitive("roundel/alabaster"));
		missingMappings.add("roundel_offset_alabaster", new JsonPrimitive("roundel/alabaster_offset"));
		
		missingMappings.add("roundel_divider", new JsonPrimitive("roundel/coral_divider"));
		missingMappings.add("roundel_coralised", new JsonPrimitive("roundel/coralised"));
		missingMappings.add("roundel_coralised_small", new JsonPrimitive("roundel/coralised_small"));
		missingMappings.add("roundel_coralised_large", new JsonPrimitive("roundel/coralised_large"));
		
		missingMappings.add("roundel_tech_wall_lamp", new JsonPrimitive("roundel/tech_wall_lamp"));
		missingMappings.add("roundel_tech_wall_lamp_offset", new JsonPrimitive("roundel/tech_wall_lamp_offset"));
		
		missingMappings.add("comfy_chair", new JsonPrimitive("comfy_chair_red"));
		
		missingMappings.add("tech_wall_dark_blank", new JsonPrimitive("tungsten"));
		missingMappings.add("tech_floor_dark_plate", new JsonPrimitive("tungsten_plate"));
		missingMappings.add("tech_floor_dark_pattern", new JsonPrimitive("tungsten_pattern"));
		missingMappings.add("tech_wall_dark_runner_lights", new JsonPrimitive("tungsten_blue_runner_lights"));
		missingMappings.add("tech_floor_dark_pipes", new JsonPrimitive("tungsten_pipes"));
		missingMappings.add("tech_floor_dark_pattern_slab", new JsonPrimitive("tungsten_pattern_slab"));
		missingMappings.add("tech_floor_dark_pattern_stairs", new JsonPrimitive("tungsten_pattern_stairs"));
		missingMappings.add("tech_floor_plate_slab", new JsonPrimitive("tungsten_plate_slab"));
		missingMappings.add("tech_floor_plate_stairs", new JsonPrimitive("tungsten_plate_stairs"));
		missingMappings.add("tech_floor_dark_pipes_slab", new JsonPrimitive("tungsten_pipes_slab"));
		missingMappings.add("tech_floor_dark_pipes_stairs", new JsonPrimitive("tungsten_pipes_stairs"));
		missingMappings.add("tech_wall_dark_blank_slab", new JsonPrimitive("tungsten_slab"));
		missingMappings.add("tech_wall_dark_blank_stairs", new JsonPrimitive("tungsten_stairs"));
		missingMappings.add("tech_floor_plate", new JsonPrimitive("tungsten_plate"));
		
		missingMappings.add("demat", new JsonPrimitive("handbrake"));
		missingMappings.add("subsystem/interstital_antenna", new JsonPrimitive("subsystem/interstitial_antenna"));
		
		missingMappings.add("japan", new JsonPrimitive("bokkusu"));
		missingMappings.add("exterior_japan", new JsonPrimitive("exterior_bokkusu"));
		missingMappings.add("key_01", new JsonPrimitive("key_gallifreyan"));
		return missingMappings;
	}
	

}
