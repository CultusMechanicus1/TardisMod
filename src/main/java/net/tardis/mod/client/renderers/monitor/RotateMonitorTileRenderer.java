package net.tardis.mod.client.renderers.monitor;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.client.models.tiles.RotateMonitorModel;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.monitors.RotateMonitorTile;

public class RotateMonitorTileRenderer extends TileEntityRenderer<RotateMonitorTile> {

    public static final WorldText TEXT = new WorldText(0.78F, 1.0F, 0.007F, 0xFFFFFF);
    public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/tiles/rotate_monitor.png");

    public RotateMonitorTileRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
        super(rendererDispatcherIn);
    }

    public static final RotateMonitorModel MODEL = new RotateMonitorModel();

    @Override
    public void render(RotateMonitorTile tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
        matrixStackIn.push();
        matrixStackIn.translate(0.5, 1.5, 0.5);
        matrixStackIn.rotate(Vector3f.ZP.rotationDegrees(180));

        MODEL.render(tile, TEXTURE, matrixStackIn, bufferIn.getBuffer(RenderType.getEntityTranslucent(TEXTURE)), partialTicks, combinedLightIn, combinedOverlayIn, 1, 1, 1, 1.0F);

        TardisHelper.getConsoleInWorld(Minecraft.getInstance().world).ifPresent(console -> {
            matrixStackIn.push();
            MODEL.translateToScreenBone(matrixStackIn);
            matrixStackIn.translate(-0.39, -0.25, -0.1525);
            TEXT.renderText(matrixStackIn, bufferIn, LightModelRenderer.MAX_LIGHT, Helper.getConsoleText(console));
            matrixStackIn.pop();
        });

        matrixStackIn.pop();
    }

}
